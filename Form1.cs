﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SortNumber
{
    public partial class Form1 : Form
    {
        List<int> list = new List<int>();
        int previousNumber;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (int.TryParse(this.textNumber.Text.ToString(), out int number))
            {
                if (number > previousNumber)
                {
                    this.labelPreviousNumber.Text = number + " > " + previousNumber;
                }
                else if(number < previousNumber)
                {
                    this.labelPreviousNumber.Text = number + " < " + previousNumber;

                }
                else
                {
                    this.labelPreviousNumber.Text = number + " = " + previousNumber;
                }

                previousNumber = number;
                this.textNumber.Text = null;
                this.list.Add(number);
                this.list.Sort();
                this.listNumbers.Items.Clear();
                foreach (int nb in list)
                {
                    this.listNumbers.Items.Add(nb);
                }
            }
            else
            {
                DialogResult dlgRes;
                dlgRes = MessageBox.Show(
                    "Sorry, you must enter a number",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Question
                );
                this.textNumber.Text = null;
            }

        }

        private void buttonChangeOrder_Click(object sender, EventArgs e)
        {
            this.list.Reverse();
            this.listNumbers.Items.Clear();
            foreach (int nb in list)
            {
                this.listNumbers.Items.Add(nb);
            }
        }
    }
}
