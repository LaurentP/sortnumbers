﻿namespace SortNumber
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonChangeOrder = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textNumber = new System.Windows.Forms.TextBox();
            this.listNumbers = new System.Windows.Forms.ListBox();
            this.labelPreviousNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonChangeOrder
            // 
            this.buttonChangeOrder.Location = new System.Drawing.Point(232, 325);
            this.buttonChangeOrder.Name = "buttonChangeOrder";
            this.buttonChangeOrder.Size = new System.Drawing.Size(84, 23);
            this.buttonChangeOrder.TabIndex = 2;
            this.buttonChangeOrder.Text = "Change order";
            this.buttonChangeOrder.UseVisualStyleBackColor = true;
            this.buttonChangeOrder.Click += new System.EventHandler(this.buttonChangeOrder_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(232, 92);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(84, 23);
            this.buttonAdd.TabIndex = 1;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.button1_Click);
            // 
            // textNumber
            // 
            this.textNumber.Location = new System.Drawing.Point(146, 94);
            this.textNumber.Name = "textNumber";
            this.textNumber.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textNumber.Size = new System.Drawing.Size(84, 20);
            this.textNumber.TabIndex = 3;
            // 
            // listNumbers
            // 
            this.listNumbers.FormattingEnabled = true;
            this.listNumbers.Location = new System.Drawing.Point(146, 120);
            this.listNumbers.Name = "listNumbers";
            this.listNumbers.Size = new System.Drawing.Size(170, 199);
            this.listNumbers.TabIndex = 0;
            // 
            // labelPreviousNumber
            // 
            this.labelPreviousNumber.AutoSize = true;
            this.labelPreviousNumber.Location = new System.Drawing.Point(143, 322);
            this.labelPreviousNumber.Name = "labelPreviousNumber";
            this.labelPreviousNumber.Size = new System.Drawing.Size(0, 13);
            this.labelPreviousNumber.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 381);
            this.Controls.Add(this.labelPreviousNumber);
            this.Controls.Add(this.textNumber);
            this.Controls.Add(this.buttonChangeOrder);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.listNumbers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "SortNumbers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonChangeOrder;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textNumber;
        private System.Windows.Forms.ListBox listNumbers;
        private System.Windows.Forms.Label labelPreviousNumber;
    }
}

